package com.hleofxquotes.md.task;

import java.util.logging.Logger;

import com.hleofxquotes.md.AbstractFeatureTask;
import com.hleofxquotes.md.logging.MxtSimpleFormatter;

public class DefaultFeatureTask extends AbstractFeatureTask {
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(DefaultFeatureTask.class.getName());

    public DefaultFeatureTask(String name, String label) {
        super(name, label, MxtFeatureTaskType.DEFAULT);
    }

    public DefaultFeatureTask(String name) {
        this(name, null);
    }

    @Override
    public void run() {
        LOGGER.info("> run - " + "name=" + getName() + ", label=" + getLabel() + ", type=" + getType() + ", uri="
                + getUri() + ", params=" + getParams());
    }

}
