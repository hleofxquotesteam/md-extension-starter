/**
 * Copyright 2018 hleofxquotes@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hleofxquotes.md.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MxtSimpleFormatter extends SimpleFormatter {

    // private static final String myFormat = "[%1$tFT%1$tT.%1$tL%1$tz]
    // %5$s%6$s%n";
    // %1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS.%1$tL %4$-7s [%3$s] (%2$s) %5$s
    // %6$s%n
    private static final String myFormat = "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS.%1$tL %4$-7s [%2$s] %5$s %6$s%n";

    private final Date myDate = new Date();

    @Override
    public synchronized String format(LogRecord record) {
        myDate.setTime(record.getMillis());

        // String source = getSource(record);
        String source = Thread.currentThread().getName();

        String message = formatMessage(record);

        String throwable = "";
        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            pw.println();
            record.getThrown().printStackTrace(pw);
            pw.close();
            throwable = sw.toString();
        }

        return String.format(myFormat, myDate, source, record.getLoggerName(), record.getLevel().getLocalizedName(),
                message, throwable);
    }

    protected String getSource(LogRecord record) {
        // com.moneydance.modules.features.rhumba.HleFeatureModule
        String source;
        if (record.getSourceClassName() != null) {
            source = record.getSourceClassName();
            source = shortenSourceName(source);
            if (record.getSourceMethodName() != null) {
                source += " " + record.getSourceMethodName();
            }
        } else {
            source = record.getLoggerName();
            source = shortenSourceName(source);
        }
        return source;
    }

    protected String shortenSourceName(String source) {
        String[] tokens = source.split("\\.");

        StringBuilder sb = new StringBuilder();
        int max = tokens.length;
        for (int i = 0; i < max; i++) {
            if (i > 0) {
                sb.append(".");
            }

            if ((i + 1) == max) {
                sb.append(tokens[i]);
            } else {
                sb.append(tokens[i].charAt(0));
            }
        }
        source = sb.toString();

        return source;
    }

    public static Logger getLogger(String name) {
        Logger logger = Logger.getLogger(name);

        configLogger(logger);

        return logger;
    }

    private static void configLogger(Logger logger) {
        // logger.setLevel(Level.INFO);
        logger.setUseParentHandlers(false);

        ConsoleHandler handler = new ConsoleHandler() {

            @Override
            public void publish(LogRecord record) {
                super.publish(record);
//                System.err.println("XXX");
            }

        };
        handler.setLevel(Level.FINEST);

        MxtSimpleFormatter newFormatter = new MxtSimpleFormatter();
        handler.setFormatter(newFormatter);

        Handler[] handlers = logger.getHandlers();
        for (Handler h : handlers) {
            logger.removeHandler(h);
        }
        
        logger.addHandler(handler);
    }

}
