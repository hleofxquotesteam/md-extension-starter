package com.hleofxquotes.md;

import java.util.HashMap;
import java.util.Map;

public class EnumAppEventTypes {
    private final Map<String, EnumAppEventType> eventTypes = new HashMap<>();

    private static final EnumAppEventTypes INSTANCE = new EnumAppEventTypes();

    public EnumAppEventTypes() {
        super();
        init();
    }

    private void init() {
        for (EnumAppEventType type : EnumAppEventType.values()) {
            eventTypes.put(type.getValue(), type);
        }
    }

    public EnumAppEventType getType(String value) {
        return eventTypes.get(value);
    }

    public static EnumAppEventTypes getInstance() {
        return INSTANCE;
    }
}
