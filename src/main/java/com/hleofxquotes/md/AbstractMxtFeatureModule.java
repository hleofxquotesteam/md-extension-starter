/**
 * Copyright 2018-2020 hleofxquotes@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hleofxquotes.md;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.swing.SwingUtilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import com.hleofxquotes.md.logging.MxtSimpleFormatter;
import com.hleofxquotes.md.task.MxtFeatureTaskType;
import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.infinitekind.moneydance.model.AccountBookListener;
import com.infinitekind.moneydance.model.AccountListener;
import com.infinitekind.moneydance.model.MDFileListener;
import com.infinitekind.moneydance.model.TransactionListener;
import com.infinitekind.moneydance.model.TransactionSet;
import com.moneydance.apps.md.controller.FeatureListener;
import com.moneydance.apps.md.controller.FeatureModule;
import com.moneydance.apps.md.controller.FeatureModuleContext;
import com.moneydance.apps.md.controller.PreferencesListener;
import com.moneydance.apps.md.view.HomePageView;
import com.moneydance.apps.md.view.MoneydanceUI;
import com.moneydance.apps.md.view.gui.MDWindowListener;
import com.moneydance.apps.md.view.gui.MainFrame;
import com.moneydance.apps.md.view.gui.MoneydanceGUI;

/**
 * The Class AbstractMxtFeatureModule.
 */
public abstract class AbstractMxtFeatureModule extends FeatureModule implements MxtEventHandler, AccountListener,
        MDFileListener, PreferencesListener, MDWindowListener, TransactionListener, FeatureListener {

    /** The Constant LOGGER. */
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(AbstractMxtFeatureModule.class.getName());

    /** The feature module context. */
//    private FeatureModuleContext featureModuleContext;

    /** The thread pool. */
    private ExecutorService threadPool;
    private ExecutorService singleThreadTaskPool;

    /** The account book listener. */
    private final AccountBookListener accountBookListener;

    private final Map<String, AbstractFeatureTask> featureTasks = new ConcurrentHashMap<>();

    private final List<HomePageView> homePageViews = new ArrayList<>();

    /**
     * Instantiates a new abstract mxt feature module.
     */
    public AbstractMxtFeatureModule() {
        accountBookListener = new AccountBookListener() {

            @Override
            public void accountBookDataReplaced(AccountBook paramAccountBook) {
                AbstractMxtFeatureModule.this.accountBookDataReplaced(paramAccountBook);
            }

            @Override
            public void accountBookDataUpdated(AccountBook paramAccountBook) {
                AbstractMxtFeatureModule.this.accountBookDataUpdated(paramAccountBook);
            }

        };
    }

    /**
     * FeatureModule lifecycle.
     */
    @Override
    public void init() {
        super.init();

        try {
            MockFeatureModuleContext.setPrivateVerified(true, this);
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }
        LOGGER.info("> init() - " + "isVerified=" + isVerified());

        File cwd = new File(".");
        System.err.println("DIRECTORY cwd=" + cwd.getAbsolutePath());

        LOGGER.info("> init - " + getModuleInfo());

        if (getThreadPool() == null) {
            setThreadPool(Executors.newCachedThreadPool());
        }
        if (getSingleThreadTaskPool() == null) {
            setSingleThreadTaskPool(Executors.newSingleThreadExecutor());
        }

        addMoneydanceGUIListeners();

        addHomePageViews();

        addFeatures();
    }

//    protected void initFeatureModuleContext() {
//        FeatureModuleContext context = getContext();
//        if (context == null) {
//            throw new RuntimeException("FeatureModuleContext is null.");
//        }
//        setFeatureModuleContext(context);
//    }

    protected void addFeatures() {
        initFeatures();

        registerFeatures();
    }

    protected void addHomePageViews() {
        initHomePageViews();

        registerHomePageViews();
    }

    private void registerHomePageViews() {
        for (HomePageView homePageView : getHomePageViews()) {
            getFeatureModuleContext().registerHomePageView(this, homePageView);
        }
    }

//    protected abstract void addHomePageViews(FeatureModuleContext context);

    protected abstract void initFeatures();

    protected abstract void initHomePageViews();

    /**
     * Cleanup.
     */
    @Override
    public void cleanup() {
        super.cleanup();
        LOGGER.fine("> cleanup");
    }

    /**
     * Unload.
     */
    @Override
    public void unload() {
        super.unload();
        LOGGER.fine("> unload");

        if (getThreadPool() != null) {
            try {
                getThreadPool().shutdownNow();
            } finally {
                setThreadPool(null);
            }
        }
        if (getSingleThreadTaskPool() != null) {
            try {
                getSingleThreadTaskPool().shutdownNow();
            } finally {
                setSingleThreadTaskPool(null);
            }
        }

        removeMoneydanceGUIListeners();
    }

    /**
     * Register features.
     */
    private void registerFeatures() {
        Map<String, AbstractFeatureTask> map = getFeatureTasks();

        for (String key : map.keySet()) {
            AbstractFeatureTask value = map.get(key);
            registerFeature(key, value.getLabel());
        }
    }

    /**
     * Register feature.
     *
     * @param feature the feature
     */
    private void registerFeature(String feature, String label) {
        try {
            if (!StringUtils.isBlank(label)) {
                final String extensionName = feature;
                final String buttonText = label;
                final FeatureModule featureModule = this;
                final Image buttonImage = getIcon(extensionName);
                getFeatureModuleContext().registerFeature(featureModule, extensionName, buttonImage, buttonText);
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Adds the account book listeners.
     */
    protected void addAccountBookListeners() {
        FeatureModuleContext context = getFeatureModuleContext();
        if (context == null) {
            return;
        }

        AccountBook book = context.getCurrentAccountBook();
        if (book != null) {
            book.addAccountListener(this);

            book.addFileListener(this);

            book.addListener(accountBookListener);

            book.getTransactionSet().addTransactionListener(this);
        }
    }

    /**
     * Removes the account book listeners.
     */
    protected void removeAccountBookListeners() {
        FeatureModuleContext context = getFeatureModuleContext();
        if (context == null) {
            return;
        }
        AccountBook book = context.getCurrentAccountBook();
        if (book != null) {
            book.removeAccountListener(this);

            book.removeFileListener(this);

            book.removeListener(accountBookListener);

            book.getTransactionSet().removeTransactionListener(this);
        }
    }

    /**
     * Adds the moneydance GUI listeners.
     */
    private void addMoneydanceGUIListeners() {
        MoneydanceGUI gui = getMoneydanceGUI();
        if (gui != null) {
            gui.addWindowListener(this);
            gui.getPreferences().addListener(this);
        }
    }

    /**
     * Removes the moneydance GUI listeners.
     */
    private void removeMoneydanceGUIListeners() {
        MoneydanceGUI gui = getMoneydanceGUI();
        if (gui != null) {
            gui.removeWindowListener(this);
            gui.getPreferences().removeListener(this);
        }
    }

    /**
     * Account book data updated.
     *
     * @param paramAccountBook the param account book
     */
    // AccountBookListener
    protected abstract void accountBookDataUpdated(AccountBook paramAccountBook);

    /**
     * Account book data replaced.
     *
     * @param paramAccountBook the param account book
     */
    protected abstract void accountBookDataReplaced(AccountBook paramAccountBook);

    /**
     * Gets the module info.
     *
     * @return the module info
     */
    private String getModuleInfo() {
        StringBuilder sb = new StringBuilder();

        sb.append("Name: " + this.getName());
        sb.append(", Build: " + this.getBuild());

        return sb.toString();
    }

    /**
     * Gets the icon.
     *
     * @param extensionName the extension name
     * @return the icon
     */
    /*
     * Get Icon is not really needed as Icons are not used. Included as the register
     * feature method requires it
     */
    protected Image getIcon(String extensionName) {
        try {
            ClassLoader cl = getClass().getClassLoader();
            java.io.InputStream in = cl
                    .getResourceAsStream("/com/moneydance/modules/features/" + extensionName + "/iconip.gif");
            if (in != null) {
                ByteArrayOutputStream bout = new ByteArrayOutputStream(1000);
                byte buf[] = new byte[256];
                int n = 0;
                while ((n = in.read(buf, 0, buf.length)) >= 0)
                    bout.write(buf, 0, n);
                return Toolkit.getDefaultToolkit().createImage(bout.toByteArray());
            }
        } catch (Throwable e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        return null;
    }

    /**
     * Gets the feature module context.
     *
     * @return the feature module context
     */
    protected FeatureModuleContext getFeatureModuleContext() {
        return getContext();
    }

    /**
     * This is the handler for showUrl() call. Once we received a request, we will
     * hand it off to a BACKGROUND (not UI) thread so that we can get off the caller
     * thread.
     *
     * @param task the task
     */
    protected void invokeFeature(AbstractFeatureTask task) {
        MxtFeatureTaskType type = task.getType();
        switch (type) {
        case UI:
            SwingUtilities.invokeLater(task);
            break;
        case NOT_THREAD_SAFE:
            getSingleThreadTaskPool().execute(task);
            break;
        case DEFAULT:
            getThreadPool().execute(task);
            break;
        }
    }

    /**
     * Gets the extension name.
     *
     * @return the extension name
     */
    protected abstract String getExtensionName();

    /**
     * Handle event.
     *
     * @param appEvent the app event
     */
    @Override
    public void handleEvent(String appEvent) {
        super.handleEvent(appEvent);

        LOGGER.fine("> handleEvent, appEvent=" + appEvent + "," + " isVerified=" + isVerified());

        EnumAppEventType type = EnumAppEventTypes.getInstance().getType(appEvent);
        if (type != null) {
            handleEvent(type);
        } else {
            LOGGER.severe("Unknown appEvent=" + appEvent);
        }
    }

    /**
     * Handle event.
     *
     * @param type the type
     */
    protected void handleEvent(EnumAppEventType type) {
        switch (type) {
        case AccountRoot:
            handleEventAccountRoot();
            break;
        case AccountSelect:
            handleEventAccountSelect();
            break;
        case AppExiting:
            handleEventAppExiting();
            break;
        case FileClosed:
            handleEventFileClosed();
            break;
        case FileClosing:
            handleEventFileClosing();
            break;
        case FileOpened:
            handleEventFileOpened();
            break;
        case FileOpening:
            handleEventFileOpening();
            break;
        case FilePostSave:
            handleEventFilePostSave();
            break;
        case FilePreSave:
            handleEventFilePreSave();
            break;
        case GraphReport:
            handleEventGraphReport();
            break;
        case LicenseUpdated:
            handleEventLicenseUpdated();
            break;
        case ViewBudget:
            handleEventViewBudget();
            break;
        case ViewReminders:
            handleEventViewReminders();
            break;
        }
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    @Override
    public String getName() {
        return getExtensionName();
    }

    /**
     * Invoke.
     *
     * @param uriString the uri string
     */
    @Override
    public void invoke(String uriString) {
        if (LOGGER.isLoggable(Level.FINER)) {
            LOGGER.finer("INVOKE, uri=" + uriString);
        }
        // getQuote?s=IBM&s=AAPL

        try {
            String url = "http://localhost/" + uriString;
            URI uri = new URI(url);
            List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");

            // String command = getCommand(uriString);
            String command = uri.getPath();
            // /getQuote
            command = command.replaceAll("/", "");
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.fine("INVOKE, command=" + command);

                for (NameValuePair param : params) {
                    if (LOGGER.isLoggable(Level.FINER)) {
                        LOGGER.finer("INVOKE, param - " + param.getName() + ": " + param.getValue());
                    }
                }
            }

            AbstractFeatureTask task = getFeatureTasks().get(command);
            if (task != null) {
                task.init(uri, params);
                invokeFeature(task);
            } else {
                LOGGER.log(Level.SEVERE, "Cannot find featureTask for feature=" + command);
            }

        } catch (URISyntaxException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * Gets the thread pool.
     *
     * @return the thread pool
     */
    protected ExecutorService getThreadPool() {
        return threadPool;
    }

    /**
     * Sets the thread pool.
     *
     * @param threadPool the new thread pool
     */
    protected void setThreadPool(ExecutorService threadPool) {
        this.threadPool = threadPool;
    }

    /**
     * Gets the root account.
     *
     * @return the root account
     */
    public Account getRootAccount() {
        return getFeatureModuleContext().getRootAccount();
    }

    /**
     * Gets the current account book.
     *
     * @return the current account book
     */
    public AccountBook getCurrentAccountBook() {
        return getFeatureModuleContext().getCurrentAccountBook();
    }

    /**
     * Gets the moneydance GUI.
     *
     * @return the moneydance GUI
     */
    public MoneydanceGUI getMoneydanceGUI() {
        MoneydanceGUI gui = null;

        FeatureModuleContext context = getFeatureModuleContext();

        if (context instanceof com.moneydance.apps.md.controller.Main) {
            com.moneydance.apps.md.controller.Main main = (com.moneydance.apps.md.controller.Main) context;
            MoneydanceUI ui = main.getUI();
            if (ui instanceof MoneydanceUI) {
                gui = (MoneydanceGUI) ui;
            }
        }

        return gui;
    }

    /**
     * Gets the transactions stream.
     *
     * @return the transactions stream
     */
    public Stream<AbstractTxn> getTransactionsStream() {
        AccountBook book = getCurrentAccountBook();
        TransactionSet transactions = book.getTransactionSet();
        Spliterator<AbstractTxn> spliterator = Spliterators.spliteratorUnknownSize(transactions.iterator(),
                Spliterator.ORDERED);
        Stream<AbstractTxn> stream = StreamSupport.stream(spliterator, false);
        return stream;
    }

    public Map<String, AbstractFeatureTask> getFeatureTasks() {
        return featureTasks;
    }

    public MainFrame getMainFrame() {
        FeatureModuleContext context = getFeatureModuleContext();
        MainFrame mainFrame = null;
        if (context instanceof com.moneydance.apps.md.controller.Main) {
            com.moneydance.apps.md.controller.Main main = (com.moneydance.apps.md.controller.Main) context;
            MoneydanceUI ui = main.getUI();
            if (ui instanceof MoneydanceGUI) {
                MoneydanceGUI gui = (MoneydanceGUI) ui;
                mainFrame = gui.getFirstMainFrame();
            }
        }
        return mainFrame;
    }

    protected void addFeatureTask(AbstractFeatureTask task) {
        getFeatureTasks().put(task.getName(), task);
    }

    /**
     * Always fire event in a background thread in case other module is take its
     * time to service the callback.
     * 
     * @param url         is the URL to callback
     * @param swingThread if true, scheduled in UI thread.
     */
    protected void fireEvent(final String eventName, final String url, boolean swingThread) {
        Runnable command = new Runnable() {
            @Override
            public void run() {
                if (LOGGER.isLoggable(Level.FINER)) {
                    LOGGER.finer("FIRE_EVENT, event=" + eventName + ", url=" + url);
                }
                getFeatureModuleContext().showURL(url);
            }
        };

        LOGGER.info("> fireEvent, event=" + eventName + ", url=" + url);
        if (swingThread) {
            SwingUtilities.invokeLater(command);
        } else {
            getThreadPool().execute(command);
        }
    }

    protected void fireEvent(String eventName, String url) {
        boolean swingThread = true;
        fireEvent(eventName, url, swingThread);
    }

    protected void setConsoleStatus(String message) {
        LOGGER.info("> setConsoleStatus: " + message);
        String url = "moneydance:setprogress?label=" + message;
        fireEvent("setConsoleStatus", url);
    }

    public AccountBookListener getAccountBookListener() {
        return accountBookListener;
    }

    public List<HomePageView> getHomePageViews() {
        return homePageViews;
    }

    @Override
    public int getBuild() {
        FeatureModuleContext context = getFeatureModuleContext();
        if (context == null) {
            context = getContext();
        }
        return context.getBuild();
    }

    public void sendRequest(String eventName, String toModule, String command, NameValuePair... params) {
        String queryString = toQueryString(params);
        String url = String.format("moneydance" + ":" + "fmodule" + ":" + "%s" + ":" + "%s", toModule,
                command + queryString);
        fireEvent(eventName, url);
    }

    private static final String toQueryString(NameValuePair... params) {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        for (NameValuePair param : params) {
            if (count > 0) {
                sb.append("&");
                sb.append(param.getName());
                sb.append("=");
                sb.append(param.getValue());
            } else {
                sb.append("?");
                sb.append(param.getName());
                sb.append("=");
                sb.append(param.getValue());
            }
            count++;
        }
        String queryString = sb.toString();
        return queryString;
    }

    public ExecutorService getSingleThreadTaskPool() {
        return singleThreadTaskPool;
    }

    public void setSingleThreadTaskPool(ExecutorService singleThreadTaskPool) {
        this.singleThreadTaskPool = singleThreadTaskPool;
    }

}
