package com.hleofxquotes.md.task;

import java.util.logging.Logger;

import com.hleofxquotes.md.AbstractFeatureTask;
import com.hleofxquotes.md.logging.MxtSimpleFormatter;

public class UiFeatureTask extends AbstractFeatureTask {
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(UiFeatureTask.class.getName());

    public UiFeatureTask(String name, String label) {
        super(name, label, MxtFeatureTaskType.UI);
    }

    @Override
    public void run() {
        LOGGER.info("> doTask - " + "name=" + getName() + ", label=" + getLabel() + ", type="
                + getType() + ", uri=" + getUri() + ", params=" + getParams());
    }

}
