package com.hleofxquotes.md;

public interface MxtEventHandler {
    void handleEventViewReminders();

    void handleEventViewBudget();

    void handleEventLicenseUpdated();

    void handleEventGraphReport();

    void handleEventFilePreSave();

    void handleEventFilePostSave();

    void handleEventFileClosing();

    void handleEventAppExiting();

    void handleEventAccountSelect();

    void handleEventAccountRoot();

    void handleEventFileOpening();

    void handleEventFileOpened();

    void handleEventFileClosed();
}
