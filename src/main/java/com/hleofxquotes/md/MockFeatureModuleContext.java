package com.hleofxquotes.md;

import java.awt.Image;
import java.io.IOException;
import java.lang.reflect.Field;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.moneydance.apps.md.controller.AccountBookWrapper;
import com.moneydance.apps.md.controller.FeatureModule;
import com.moneydance.apps.md.controller.FeatureModuleContext;
import com.moneydance.apps.md.extensionapi.AccountEditor;
import com.moneydance.apps.md.view.HomePageView;

public class MockFeatureModuleContext implements FeatureModuleContext {
    private static final Log LOGGER = LogFactory.getLog(MockFeatureModuleContext.class);

    private final AccountBookWrapper accountBookWrapper;

    private final MetaInfo metaInfo;

    public MockFeatureModuleContext(AccountBookWrapper accountBookWrapper, MetaInfo metaInfo) throws IOException {
        this.accountBookWrapper = accountBookWrapper;
        this.metaInfo = metaInfo;
    }

    @Override
    public void showURL(String arg0) {
        LOGGER.info("> showURL");

    }

    @Override
    public void registerHomePageView(FeatureModule arg0, HomePageView arg1) {
        LOGGER.info("> registerHomePageView");
    }

    @Override
    public void registerFeature(FeatureModule arg0, String arg1, Image arg2, String arg3) {
        LOGGER.info("> registerFeature");
    }

    @Override
    public void registerAccountEditor(FeatureModule arg0, int arg1, AccountEditor arg2) {
        LOGGER.info("> registerAccountEditor");
    }

    @Override
    public String getVersion() {
        LOGGER.info("> getVersion");
        return "1.0.0";
    }

    @Override
    public Account getRootAccount() {
        LOGGER.info("> getRootAccount");
        Account rootAccount = null;
        if (this.accountBookWrapper != null) {
            rootAccount = this.accountBookWrapper.getBook().getRootAccount();
        }
        return rootAccount;
    }

    @Override
    public AccountBook getCurrentAccountBook() {
        LOGGER.info("> getCurrentAccountBook");
        AccountBook accountBook = null;
        if (this.accountBookWrapper != null) {
            accountBook = this.accountBookWrapper.getBook();
        }
        return accountBook;
    }

    @Override
    public int getBuild() {
        LOGGER.info("> getBuild");
        int build = 1;
        MetaInfo metaInfo = getMetaInfo();
        if (metaInfo != null) {
            try {
                build = Integer.valueOf(metaInfo.getModuleBuild());
            } catch (NumberFormatException e) {
                LOGGER.error(e.getMessage());
                build = 1;
            }
        }
        return build;
    }

    public AccountBookWrapper getAccountBookWrapper() {
        return accountBookWrapper;
    }

    public MetaInfo getMetaInfo() {
        return metaInfo;
    }

    public static final void setPrivateContext(FeatureModule featureModule)
            throws IOException, NoSuchFieldException, IllegalAccessException {
        MetaInfo metaInfo = new MetaInfo(featureModule.getClass());
        AccountBookWrapper accountBookWrapper = null;
        FeatureModuleContext mockFeatureModuleContext = new MockFeatureModuleContext(accountBookWrapper, metaInfo);
        MockFeatureModuleContext.setPrivateContext(mockFeatureModuleContext, featureModule);
    }

    private static void setPrivateContext(FeatureModuleContext mockFeatureModuleContext, FeatureModule featureModule)
            throws NoSuchFieldException, IllegalAccessException {
        Field field = FeatureModule.class.getDeclaredField("context");
        field.setAccessible(true);
        field.set(featureModule, mockFeatureModuleContext);
    }

    public static void setPrivateVerified(boolean b, FeatureModule featureModule)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        Field field = FeatureModule.class.getDeclaredField("isVerified");
        field.setAccessible(true);
        field.set(featureModule, b);
    }

}