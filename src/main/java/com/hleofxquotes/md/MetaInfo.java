package com.hleofxquotes.md;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

import com.hleofxquotes.md.logging.MxtSimpleFormatter;
import com.infinitekind.util.StreamTable;
import com.infinitekind.util.StringEncodingException;

public class MetaInfo {
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(MetaInfo.class.getName());

    private static final String META_INFO_DICT = "meta_info.dict";

    private static final String KEY_MODULE_NAME = "module_name";
    private static final String KEY_MODULE_BUILD = "module_build";
    private static final String KEY_VENDOR = "vendor";

    private final String moduleName;
    private final String moduleBuild;

    private final String vendor;

//    "vendor" = "Mike Bray"
//            "module_build" = "3009"
//            "minbuild" = "1820"
//            "vendor.url" = "http://bitbucket.org/mikerb/moneydance-2015wiki/Home/"
//            "module_name" = "Budget Generator"
//            "module_desc" = "<html><body><title>Budget Generator</title>><p>Generates entries for the new style budgets from budget lines based on period of expense, amount and first date of payment
//          </body></html>"
//            "desc" = "Takes entries for each expense category and calculates when payments will be made.\n Creates budget entries to match the selected budget period.\n"

    public <T> MetaInfo(Class<T> clz) throws IOException {
        String name = META_INFO_DICT;
        try (InputStream stream = clz.getResourceAsStream(name)) {
            if (stream != null) {
                StreamTable streamTable = new StreamTable();
                try {
                    streamTable.readFrom(stream);
                    this.moduleName = streamTable.getStr(KEY_MODULE_NAME, null);
                    LOGGER.info("moduleName=" + moduleName);
                    
                    this.moduleBuild = streamTable.getStr(KEY_MODULE_BUILD, null);
                    LOGGER.info("moduleBuild=" + moduleBuild);

                    this.vendor = streamTable.getStr(KEY_VENDOR, null);
                    LOGGER.info("vendor=" + vendor);
                } catch (StringEncodingException e) {
                    throw new IOException(e);
                }
            } else {
                throw new IOException("Cannot find resource=" + name);
            }
        }
    }

    public String getModuleName() {
        return moduleName;
    }

    public String getModuleBuild() {
        return moduleBuild;
    }

    public String getVendor() {
        return vendor;
    }
}
