package com.hleofxquotes.md;

import java.net.URI;
import java.util.List;

import org.apache.http.NameValuePair;

import com.hleofxquotes.md.task.MxtFeatureTaskType;

public abstract class AbstractFeatureTask implements Runnable {
    private final String name;
    private final String label;
    private final MxtFeatureTaskType type;
    private URI uri;
    private List<NameValuePair> params;

    public AbstractFeatureTask(String name, String label, MxtFeatureTaskType type) {
        super();
        this.name = name;
        this.label = label;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public URI getUri() {
        return uri;
    }

    public List<NameValuePair> getParams() {
        return params;
    }

    public MxtFeatureTaskType getType() {
        return type;
    }

    protected void init(URI uri, List<NameValuePair> params) {
        this.uri = uri;
        this.params = params;
    }

}
