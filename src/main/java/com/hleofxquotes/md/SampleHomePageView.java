package com.hleofxquotes.md;

import java.awt.BorderLayout;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.hleofxquotes.md.logging.MxtSimpleFormatter;
import com.infinitekind.moneydance.model.AccountBook;
import com.moneydance.apps.md.controller.FeatureModule;
import com.moneydance.apps.md.controller.FeatureModuleContext;
import com.moneydance.apps.md.view.HomePageView;
import com.moneydance.apps.md.view.gui.MoneydanceGUI;
import com.moneydance.apps.md.view.gui.MoneydanceLAF;

public final class SampleHomePageView implements HomePageView {
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(SampleHomePageView.class.getName());

    private final MoneydanceGUI gui;
    private FeatureModule featureModule;
    private FeatureModuleContext context;

    private final String id = this.getClass().getName();

    private MyView view;

    private boolean active;

    private class MyView extends JPanel {
        private JLabel label;

        private AccountBook accountBook;

        public MyView(AccountBook accountBook) {
            this.accountBook = accountBook;

            setLayout(new BorderLayout());
            setBorder(MoneydanceLAF.homePageBorder);
            setOpaque(false);

            label = new JLabel();

            add(label, BorderLayout.CENTER);

            if (active) {
                activate();
            }
        }

        public void refresh() {
            Runnable doRun = new Runnable() {

                @Override
                public void run() {
                    label.setText("HomePageView was added by" + " extension=" + featureModule.getName()
                            + ", getTransactionCount=" + accountBook.getTransactionSet().getTransactionCount());
                }
            };
            SwingUtilities.invokeLater(doRun);
        }

        public void activate() {
            refresh();
        }

        public void deactivate() {
        }

    }

    public SampleHomePageView(MoneydanceGUI gui, FeatureModule featureModule, FeatureModuleContext context) {
        this.gui = gui;
        this.featureModule = featureModule;
        this.context = context;
    }

    @Override
    public String toString() {
        return "SampleHomePageView";
    }

    @Override
    public JComponent getGUIView(AccountBook accountBook) {
        LOGGER.info("> getGUIView, accountBook=" + accountBook);

        if (view != null) {
            return view;
        }

        synchronized (this) {
            if (view == null) {
                view = new MyView(accountBook);
            }
            return view;
        }
    }

    @Override
    public String getID() {
        LOGGER.info("> getID");
        return id;
    }

    @Override
    public void refresh() {
        LOGGER.info("> refresh");
        if (view != null) {
            view.refresh();
        }
    }

    @Override
    public synchronized void reset() {
        LOGGER.info("> reset");
        this.setActive(false);
        this.view = null;
    }

    @Override
    public void setActive(boolean active) {
        LOGGER.info("> setActive, active=" + active);
        this.active = active;
        if (view != null) {
            if (active) {
                view.activate();
            } else {
                view.deactivate();
            }
        }
    }
}