package com.hleofxquotes.md.task;

public enum MxtFeatureTaskType {
    // Task is thread safe so we will execute them in multiple threads
    DEFAULT, 
    // Task needed to be execute using the UI queue
    UI, 
    // Task is NOT thread safe. So we will execute them serially
    NOT_THREAD_SAFE;
}
