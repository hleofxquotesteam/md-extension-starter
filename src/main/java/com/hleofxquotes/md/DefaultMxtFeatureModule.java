package com.hleofxquotes.md;

import java.util.logging.Logger;
import java.util.stream.Stream;

import com.hleofxquotes.md.logging.MxtSimpleFormatter;
import com.infinitekind.moneydance.model.AbstractTxn;
import com.infinitekind.moneydance.model.Account;
import com.infinitekind.moneydance.model.AccountBook;
import com.moneydance.apps.md.view.gui.SecondaryWindow;

public class DefaultMxtFeatureModule extends AbstractMxtFeatureModule {
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(DefaultMxtFeatureModule.class.getName());
    private String extensionName;

    public DefaultMxtFeatureModule() {
        super();
        
        initExtensionName();
    }

    // MxtEventHandler
    // https://bitbucket.org/mikerb/moneydance-2019/wiki/Application%20Events
    @Override
    public void handleEventViewReminders() {
        LOGGER.fine("> handleEventViewReminders");
    }

    @Override
    public void handleEventViewBudget() {
        LOGGER.fine("> handleEventViewBudget");
    }

    @Override
    public void handleEventLicenseUpdated() {
        LOGGER.fine("> handleEventLicenseUpdated");
    }

    @Override
    public void handleEventGraphReport() {
        LOGGER.fine("> handleEventGraphReport");
    }

    @Override
    public void handleEventFilePreSave() {
        LOGGER.fine("> handleEventFilePreSave");
    }

    @Override
    public void handleEventFilePostSave() {
        LOGGER.fine("> handleEventFilePostSave");
    }

    @Override
    public void handleEventFileClosing() {
        LOGGER.fine("> handleEventFileClosing");
    }

    @Override
    public void handleEventAppExiting() {
        LOGGER.fine("> handleEventAppExiting");
    }

    @Override
    public void handleEventAccountSelect() {
        LOGGER.fine("> handleEventAccountSelect");
    }

    /**
     * The root account has been selected
     */
    @Override
    public void handleEventAccountRoot() {
        Account rootAccount = getRootAccount();
        LOGGER.fine("> handleEventAccountRoot, rootAccount=" + rootAccount);

        Stream<AbstractTxn> stream = getTransactionsStream();
        stream.filter(txn -> {
            return true;
        }).forEach((txn) -> {
            LOGGER.fine("txn=" + txn);
        });
    }

    @Override
    public void handleEventFileOpening() {
        LOGGER.fine("> handleEventFileOpening");
    }

    @Override
    public void handleEventFileOpened() {
        LOGGER.fine("> handleEventFileOpened");
        addAccountBookListeners();
    }

    @Override
    public void handleEventFileClosed() {
        LOGGER.fine("> handleEventFileClosed");
        removeAccountBookListeners();
    }

    // AccountListener
    @Override
    public void accountModified(Account paramAccount) {
        LOGGER.fine("> accountModified, paramAccount=" + paramAccount);
    }

    @Override
    public void accountBalanceChanged(Account paramAccount) {
        LOGGER.fine("> accountBalanceChanged, paramAccount=" + paramAccount);
    }

    @Override
    public void accountDeleted(Account paramAccount1, Account paramAccount2) {
        LOGGER.fine("> accountDeleted, paramAccount1=" + paramAccount1 + ", paramAccount2=" + paramAccount2);
    }

    @Override
    public void accountAdded(Account paramAccount1, Account paramAccount2) {
        LOGGER.fine("> accountAdded, paramAccount1=" + paramAccount1 + ", paramAccount2=" + paramAccount2);
    }

    // MDFileListener
    @Override
    public void dirtyStateChanged(Account paramAccount) {
        LOGGER.fine("> dirtyStateChanged, paramAccount=" + paramAccount);
    }

    // AccountBookListener
    @Override
    protected void accountBookDataUpdated(AccountBook paramAccountBook) {
        LOGGER.fine("> accountBookDataUpdated, paramAccountBook=" + paramAccountBook);
    }

    @Override
    protected void accountBookDataReplaced(AccountBook paramAccountBook) {
        LOGGER.fine("> accountBookDataReplaced, paramAccountBook=" + paramAccountBook);
    }

    // PreferencesListener
    @Override
    public void preferencesUpdated() {
        LOGGER.fine("> preferencesUpdated");
    }

    // MDWindowListener
    @Override
    public void windowAdded(SecondaryWindow paramSecondaryWindow) {
        LOGGER.fine("> windowAdded, paramSecondaryWindow=" + paramSecondaryWindow);
    }

    @Override
    public void windowRemoved(SecondaryWindow paramSecondaryWindow) {
        LOGGER.fine("> windowRemoved, paramSecondaryWindow=" + paramSecondaryWindow);
    }

    // TransactionListener
    @Override
    public void transactionAdded(AbstractTxn paramAbstractTxn) {
        LOGGER.fine("> transactionAdded, paramAbstractTxn=" + paramAbstractTxn);
    }

    @Override
    public void transactionModified(AbstractTxn paramAbstractTxn) {
        LOGGER.fine("> transactionModified, paramAbstractTxn=" + paramAbstractTxn);
    }

    @Override
    public void transactionRemoved(AbstractTxn paramAbstractTxn) {
        LOGGER.fine("> transactionRemoved, paramAbstractTxn=" + paramAbstractTxn);
    }

    // FeatureListener
    @Override
    public void featureListModified() {
        LOGGER.fine("> featureListModified");
    }

    @Override
    protected void initFeatures() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void initHomePageViews() {
        // TODO Auto-generated method stub

    }

    /**
     * Gets the extension name.
     *
     * @return the extension name
     */
    protected String getExtensionName() {
        return this.extensionName;
    }

    protected void initExtensionName() {
        this.extensionName = DefaultMxtFeatureModule.toExtensionName(this.getClass());
    }

    public static String toExtensionName(String packageName) {
        LOGGER.info("packageName=" + packageName);

        String extensionName = packageName;
        String[] tokens = packageName.split("\\.");
        if (tokens.length > 0) {
            extensionName = tokens[tokens.length - 1];
        }

        LOGGER.info("extensionName=" + extensionName);
        return extensionName;
    }

    public static <T> String toExtensionName(Class<T> clz) {
        return DefaultMxtFeatureModule.toExtensionName(clz.getPackageName());
    }

}
