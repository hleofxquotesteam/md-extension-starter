package com.hleofxquotes.md;

import java.io.File;

public class MxtUtils {
    public static void enablePortableMain(String[] args) {
        // Make it portable by setting home directory to current directory
        File cwd = new File(".");
        File homeDir = new File(cwd, "moneyDance");
        homeDir.mkdirs();

        String userHomeDirectory = homeDir.getAbsolutePath();
        System.setProperty("user.home", userHomeDirectory);
        System.out.println("user.home=" + System.getProperty("user.home"));
    }
}
