package com.hleofxquotes.md;

// https://bitbucket.org/mikerb/moneydance-2019/wiki/Application%20Events
public enum EnumAppEventType {
//    md:file:closing, // The Moneydance file is being closed
    FileClosing("md:file:closing"),
//    md:file:closed, //  The Moneydance file has closed
    FileClosed("md:file:closed"),
//    md:file:opening, // The Moneydance file is being opened
    FileOpening("md:file:opening"),
//    md:file:opened, //  The Moneydance file has opened
    FileOpened("md:file:opened"),
//    md:file:presave, // The Moneydance file is about to be saved
    FilePreSave("md:file:presave"),
//    md:file:postsave, //    The Moneydance file has been saved
    FilePostSave("md:file:postsave"),
//    md:app:exiting, //  Moneydance is shutting down
    AppExiting("md:app:exiting"),
//    md:account:select, //   An account has been selected by the user
    AccountSelect("md:account:select"),
//    md:account:root, // The root account has been selected
    AccountRoot("md:account:root"),
//    md:graphreport, //  An embedded graph or report has been selected
    GraphReport("md:graphreport"),
//    md:viewbudget, //   One of the budgets has been selected
    ViewBudget("md:viewbudget"),
//    md:viewreminders, //    One of the reminders has been selected
    ViewReminders("md:viewreminders"),
//    md:licenseupdated, //   The user has updated the license
    LicenseUpdated("md:licenseupdated"),;

    private final String value;

    EnumAppEventType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
