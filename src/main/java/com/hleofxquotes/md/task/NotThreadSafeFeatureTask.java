package com.hleofxquotes.md.task;

import java.util.logging.Logger;

import com.hleofxquotes.md.AbstractFeatureTask;
import com.hleofxquotes.md.logging.MxtSimpleFormatter;

public class NotThreadSafeFeatureTask extends AbstractFeatureTask {
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(NotThreadSafeFeatureTask.class.getName());

    public NotThreadSafeFeatureTask(String name, String label) {
        super(name, label, MxtFeatureTaskType.NOT_THREAD_SAFE);
    }

    public NotThreadSafeFeatureTask(String name) {
        this(name, null);
    }

    @Override
    public void run() {
        LOGGER.info("> doTask - " + "name=" + getName() + ", label=" + getLabel() + ", type=" + getType() + ", uri="
                + getUri() + ", params=" + getParams());
    }

}
