package com.hleofxquotes.md;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;

public class MxtResourceLoader {
    private final URLClassLoader classLoader;
    private final File sourceFile;

    public MxtResourceLoader(File sourceFile) throws MalformedURLException {
        this(sourceFile, true);
    }

    public MxtResourceLoader(File sourceFile, boolean noParentClassLoader) throws MalformedURLException {
        super();
        this.sourceFile = sourceFile;
        URL[] urls = new URL[1];
        urls[0] = sourceFile.toURI().toURL();

        if (noParentClassLoader) {
            this.classLoader = new URLClassLoader(urls, null);
        } else {
            this.classLoader = new URLClassLoader(urls);
        }
    }

    public URL getResource(String name) {
        return classLoader.getResource(name);
    }

    public InputStream getResourceAsStream(String name) {
        return classLoader.getResourceAsStream(name);
    }

    public Enumeration<URL> getResources(String name) throws IOException {
        return classLoader.getResources(name);
    }
}
